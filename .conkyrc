# Conky, a system monitor, based on torsmo
#
# Any original torsmo code is licensed under the BSD license
#
# All code written since the fork of torsmo is licensed under the GPL
#
# Please see COPYING for details
#
# Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
# Copyright (c) 2005-2010 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
# All rights reserved.
#
# This program is free software: you can whiteistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

alignment top_right
background yes
border_width 1
cpu_avg_samples 2
default_color green
default_outline_color blue
default_shade_color blue
draw_borders no
draw_graph_borders no
draw_outline no
draw_shades no
use_xft yes
xftfont AR PL KaitiM GB:size=12
gap_x 5
gap_y 30
minimum_size 280 5
net_avg_samples 2
no_buffers no
out_to_console no
out_to_stderr no
extra_newline no
own_window no
own_window_class Conky
own_window_type desktop
own_window_argb_visual false
own_window_argb_value 120
double_buffer yes
stippled_borders 0
update_interval 1
uppercase no
use_spacer none
show_graph_scale no
show_graph_range no

TEXT
${color red}${font AR PL KaitiM GB:style=Blod:size=17}    ${time %Y.%m.%d %H:%M:%S}
#${font AR PL KaitiM GB:style=Blod:size=12}
${font AR PL KaitiM GB:style=Blod:size=12}${scroll 16 $nodename - $sysname $kernel on $machine}
$hr
${font AR PL KaitiM GB:style=Blod:size=14}${color green}${exec cal}
$hr
${font AR PL KaitiM GB:style=Blod:size=12}${color white}Uptime:$color $uptime
${color white}Frequency (in MHz):$color $freq
${color white}Frequency (in GHz):$color $freq_g
${color white}RAM: $color$mem/$memmax - $memperc% ${membar 4}
${color white}Swap: $color$swap/$swapmax - $swapperc% ${swapbar 4}
${color white}CPU1: $color${cpu cpu1}% ${cpubar 4}
${color white}CPU2: $color${cpu cpu2}% ${cpubar 4}
${color white}Processes: $color $processes  ${color red}Running:$color $running_processes
$hr
#${color red}Temp:
${color green}CPU1 Temp: ${color red}${exec sensors | grep 'Core 0' | cut -c15-16}°C
${color green}CPU2 Temp: ${color red}${exec sensors | grep 'Core 1' | cut -c15-16}°C
${color green}System Temp: ${color red}${exec sensors | grep 'temp1' | cut -c15-16}°C
${color green}Hard Temp: ${color red}${exec hddtemp /dev/sda -n -u=C}°C
$hr
#${color red}File systems:
${color green}${exec df -h | grep /dev/sda10 | cut -c40-43} / $color${fs_used /}/${fs_size /} ${fs_bar 6 /}
${exec df -h | grep /dev/sda9 | cut -c40-43} /home $color${fs_used /home}/${fs_size /home} ${fs_bar 6 /home}
${exec df -h | grep /dev/sda8 | cut -c40-43} /boot $color${fs_used /boot}/${fs_size /boot} ${fs_bar 6 /boot}
$hr
#${color red}Networking:
${color red}eth0:
Up:$color ${upspeed eth0} ${color red} - Down:$color ${downspeed eth0}
${color red}ppp0:
${color red}Up:$color ${upspeed ppp0} ${color red} - Down:$color ${downspeed ppp0}
$hr
${color red}Name              PID   CPU%   MEM%
${color green} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${color green} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${color green} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${color white} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
${color white} ${top name 5} ${top pid 5} ${top cpu 5} ${top mem 5}
